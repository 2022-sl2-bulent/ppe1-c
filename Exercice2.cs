﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Film> films = new List<Film>();
            List<string> acteurs = new List<string>();

            // Ajout du film Inception
            acteurs.Add("Leonardo DiCaprio");
            acteurs.Add("Marion Cotillard");
            acteurs.Add("Mickael Caine");
            Film inception = new Film("Inception", "Christopher Nolan", new DateTime(2010, 09, 21), acteurs);
            films.Add(inception);
            acteurs.Clear();

            // Ajoute du film The Dark Knight Rises
            acteurs.Add("Christian Bale");
            acteurs.Add("Marion Cotillard");
            acteurs.Add("Mickael Caine");
            Film theDarkNightRises = new Film("The Dark Night Rises", "Christopher Nolan", new DateTime(2012, 07, 25), acteurs);
            films.Add(theDarkNightRises);
            acteurs.Clear();

            

            AfficherFilmsDetailles(films);
            Console.ReadKey();

        }
        static void AfficherFilmsDetailles(List<Film> films)
        {
            foreach (Film film in films)
            {
                Console.WriteLine("\n" + film.Decrire() + "\n");
            }
        }

    }
}
