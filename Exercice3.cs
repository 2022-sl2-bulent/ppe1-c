﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice3
{
    class Program
    {
        static List<Film> films = new List<Film>();
        static List<string> acteurs = new List<string>();
        static void Main(string[] args)
        {
            // Ajout du film Inception
            acteurs.Add("Leonardo DiCaprio");
            acteurs.Add("Marion Cotillard");
            acteurs.Add("Mickael Caine");
            Film inception = new Film("Inception", "Christopher Nolan", new DateTime(2010, 09, 21), acteurs);
            films.Add(inception);
            acteurs.Clear();

            // Ajoute du film The Dark Knight Rises
            acteurs.Add("Christian Bale");
            acteurs.Add("Marion Cotillard");
            acteurs.Add("Mickael Caine");
            Film theDarkNightRises = new Film("The Dark Night Rises", "Christopher Nolan", new DateTime(2012, 07, 25), acteurs);
            films.Add(theDarkNightRises);
            acteurs.Clear();


            Console.WriteLine("Ma DVDThèque");
            {
                bool sortir = false;
                while (!sortir)
                {
                    Console.WriteLine("1 - Afficher la liste des films");
                    Console.WriteLine("2 - Afficher la liste detaillées des films");
                    Console.WriteLine("3 - Chercher un film");
                    Console.WriteLine("4 - Chercher un acteur");
                    Console.WriteLine("5 - Ajouter un film");
                    Console.WriteLine("0 - Quitter la DVDThèque");
                    Console.Write("Votre choix : ");

                    int choix = Convert.ToInt16(Console.ReadLine());
                    switch (choix)
                    {
                        case 1:
                            AfficherFilms(films);
                            break;
                        case 2:
                            AfficherFilmsDetailles(films);
                            break;

                        case 3:
                            ChercherParFilm(films);
                            break;
                        case 4:
                            ChercherParActeur(films);
                            break;
                        case 5:
                            AjouterFilm(films);
                            break;
                        case 0:
                            sortir = true;
                            break;
                        default:
                            Console.WriteLine("Saisir un choix valide !!!");
                            break;

                    }
                }
            }

            Console.ReadKey();
        }

        static void AjouterFilm(List<Film> films)
        {
            Console.Write("Entrer le titre du film à ajouter : ");
            string titre = Console.ReadLine();
            Console.Write("Entrer le nom du réalisateur : ");
            string realisateur = Console.ReadLine();
            Console.Write("Entrer la date de sortie (format aaaa,mm,jj) : ");
            string date = Console.ReadLine();
            DateTime dateSortie = Convert.ToDateTime(date);
            bool finActeur = false;
            List<string> acteurs = new List<string>();
            while (!finActeur)
            {
                Console.Write("Entrer un acteur (actrice) : ");
                acteurs.Add(Console.ReadLine());
                Console.Write("Voulez vous ajouter un autre acteur (o/n) ? ");
                string finSaisieActeur = Console.ReadLine();
                if (finSaisieActeur == "n")
                {
                    finActeur = true;
                }

            }
            Film nouveauFilm = new Film(titre, realisateur, dateSortie, acteurs);
            films.Add(nouveauFilm);


        }

        static void AfficherFilmsDetailles(List<Film> films)
        {
            foreach (Film film in films)
            {
                Console.WriteLine("\n" + film.Decrire() + "\n");
            }
        }
        static void AfficherFilms(List<Film> films)
        {
            foreach (Film film in films)
            {
                Console.WriteLine(film.Titre);
            }
        }

        static void ChercherParFilm(List<Film> films)
        {
            Console.Write("Entrer le nom du film à rechercher : ");
            string filmCherche = Console.ReadLine();
            string resultat = "";
            bool trouve = false;
            foreach (Film film in films)
            {
                if (film.Titre.Contains(filmCherche))
                {
                    resultat += "\n" + film.Decrire();
                    trouve = true;

                }
            }
            if (!trouve)
            {
                resultat = "\n" + filmCherche + " ne figure pas dans la DVDTech";
            }
            Console.WriteLine(resultat);

        }

        static void ChercherParActeur(List<Film> films)
        {
            Console.Write("Entrer le prénom et nom de l'acteur à rechercher : ");
            string acteurCherche = Console.ReadLine();
            string resultat = "";
            bool trouve = false;
            foreach (Film film in films)
            {
                if (film.Acteurs.Contains(acteurCherche))
                {
                    resultat += "\n" + film.Titre;
                    trouve = true;
                }
            }
            if (!trouve)
            {
                resultat = "\n" + acteurCherche + " ne figure pas dans la DVDTech";
            }
            else
            {
                resultat = "\n" + acteurCherche + " joue dans les film(s) suivant " + resultat + "\n";
            }
            Console.WriteLine(resultat);

        }
    }
}
