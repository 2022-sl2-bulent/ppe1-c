﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice1
{
    class Film
    {
        private string titre;
        private string realisateur;
        private DateTime dateSortie;
        private List<string> acteurs;

        public string Titre { get => titre; set => titre = value; }
        public string Realisateur { get => realisateur; set => realisateur = value; }
        public List<string> Acteurs { get => acteurs; set => acteurs = value; }
        public DateTime DateSortie { get => dateSortie; set => dateSortie = value; }



        public Film(string titre, string realisateur, DateTime dateSortie, List<string> desActeurs)
        {
            this.titre = titre;
            this.realisateur = realisateur;
            this.dateSortie = dateSortie;
            this.acteurs = new List<string>();
            foreach (string acteur in desActeurs)
            {
                acteurs.Add(acteur);
            }

        }

        public string Decrire()
        {
            string description;
            description = "Film : " + this.titre + "\nRéalisé par : " + this.realisateur;
            description += "\nSortie en France le : " + this.dateSortie.ToShortDateString();
            description += "\nDistribution : ";
            foreach (string acteur in acteurs)
            {
                description += "\n\t- " + acteur;
            }
            return description;
        }
    }
}
