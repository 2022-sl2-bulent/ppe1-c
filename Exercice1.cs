﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> acteurs = new List<string>();
            acteurs.Add("Joaquin Phoenix");
            acteurs.Add("Robert De Niro");
            Film joker = new Film("Joker", "Todd Phillips", new DateTime(2019, 10, 09), acteurs);
            Console.WriteLine(joker.Decrire());

            Console.ReadKey();
        }
    }
}
